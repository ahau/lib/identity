# Digital Identity Design


## Questions

Does and individual create there own DID or is this created by the credential issuer?
- is a new one created by each individual issuer?

Can you walk us through the process for creating a DID, a DID Document for an individual?
- what does a did look like?
- what does the did document look like?
- what is written on chain?
    - where is the did stored
    - where is the did document stored
    - where are the keys stored

Can you walk us through the process of creating DID and DID Document for organisation?

Can you walk us through the process of issuing a VC by an organisation?

What are the main DID methods, how do they work, and what are their differences?
- key?
- peer?
- git?
- web?
- ion?
- others we should know about?

What are the different types of Verifiable Data Registries, how do they work? and what are their differences?
- Blockchain
- Centralised database server
- IPFS

Is there a benefit to DID Documents on chain vs off chain?

What goes on chain and what doesn't?

With proof "type": "Ed25519Signature2020" what is the detached copy/string to generate the proofValue?

Is there a time-bound challenge around proving ownership of a VC?

When should DID's and VC's be used? 

When shouldn't they be used?

---

## Ahau Proposed Adoption of DID's + VC's 

### A Mana Enhancing Approach to Digital Identity 

**Individuals**
- Moana
- Matua
- Hemi

**Representative Entities**
- Iwi
- Hapu
- Govt
- Banks

**Crendentials**
- Member
- Kaitiaki
- Kaumatua
- Tribal Authority
- Passport
- KYC/AML

```mermaid
sequenceDiagram
  autonumber
  
  actor Moana
      Note over Moana: Downloads wallet<br/> and sets up DID 
  participant Iwi as Ngati Iwi
  Moana ->> Iwi: Sets up tribal registration process
  Note over Iwi: Creates a DID for Iwi <br/> with Moana as the Controller
  
  Iwi --X Moana: Issues Member VC
  Iwi --X Moana: Issues Kaitiaki VC
  Note over Moana: Stores Member and <br/>Kaitiaki VC's
  
  Moana --X Iwi: Issues Ngati Iwi Tribal Authority VC
  Note over Moana, Iwi: As controller of Tribal DID <br/>Moana Stores Tribal Authority VC
  
  actor Matua
  Note over Matua: Downloads wallet<br/> and sets up DID
  participant Hapu as Ngati Hapu
  Matua ->> Hapu: Sets up tribal registration process
  Note over Hapu: Creates a DID for Hapu <br/> with Matua as the Controller
  Hapu --X Matua: Issues Member VC
  Hapu --X Matua: Issues Kaitiaki VC
  Hapu --X Matua: Issues Kaumatua VC
  Note over Matua: Stores Member, Kaitiaki<br/> and Kaumatua VC's
  
  Matua --X Hapu: Issues Tribal Authority VC
  Note over Hapu, Matua: As controller of Tribal DID <br/>Matua Stores Tribal Authority VC
  
  Iwi --X Hapu: Issues Tribal Authority VC
  Note over Hapu, Matua: Stores Tribal Authority VC
  Note over Hapu, Matua: Stores Ngati Iwi DID for VC Verification
  Hapu --X Iwi: Issues Tribal Authority VC
  Note over Iwi, Moana: Stores Tribal Authority VC
  Note over Iwi, Moana: Stores Ngati Hapu DID for VC Verification
  
  actor Hemi
  Note over Hemi: Downloads wallet<br/> and sets up DID 
  Hemi ->> Hapu: Registers with Hapū
  Hemi --x Hapu: Issues Tribal Authority VC
  Note over Hapu, Matua: Stores Tribal Authority VC
  Hemi --X Matua: Issues Kaumatua VC
  Note over Matua: Stores Kaumatua VC
  Hapu ->> Matua: Matua recieves registration request
  Note over Hapu: Verifies tribal member information
  Matua ->> Hapu: Approves registration
  Hapu --x+ Hemi: Issues Member VC
  
  Hemi ->>- Iwi: Registers with Iwi and presents Ngati Hapū issued Member VC
  Hemi --X Iwi: Issues Tribal Authority VC
  Note over Iwi: Iwi verifies member VC <br/> and recognises  Ngati Hapū DID 
  Iwi --X+ Hemi: Issues Member VC

  participant Gov
 
  Hemi ->>_ Gov: registers for service by presenting Iwi + Hapu VC
  Note over Gov: verifies tribal VC
  Gov --x+ Hemi: Sends Passport VC

  participant Bank
 
  Hemi ->>- Bank: Applies to open bank account by presenting Iwi + Gov VC
  Note over Bank: verifies tribal VC
  Bank ->> Hemi: opens new account
  Bank --X Hemi: Issues KYC/AML VC
  
```

### Description

In this diagram you can see that that each individual and entity is an equal participant in the digital identity system. Claims by the entity and individual are both valuable in establishing a reputation of trust. Each providing recognition of each others identity.

This approaches focuses on the the value of the network for establishing authority.

The more people that recognise a Tribal Authority and/or Kaumatua the more authority or trust that entity will have in the claims that it makes.

Where Iwi or Hapu's authority is recognised and determined by the people, not an official ledger or process. 

This also takes into account the claims for people that have recognised roles in a community


---

## DID Process
1. Member installs wallet app and setups up wallet
    * this generates a private/public key pair
        * this information is saved on the wallet device
```
{
    "curve": "ed25519",
    "public": "exampleHemisPublicKeyString.ed25519",
    "private": "exampleHemisPrivateKeyString.ed25519",
    "id": "@exampleHemisPublicKeyString=.ed25519"
}
```
       
    
    * this generates a DID

`did:example:hemisDIDIdentitySting`

    * this generates DID document

```
    {
      // DID Syntax did:method:uniqueIdentifierString
      "id": "did:example:hemisDIDIdentitySting",
      
      // used to authenticate or authorize interactions with the DID subject
      "verificationMethod": [{
          
        // A unique identifier for this verification method
        "id": "did:example:hemisDIDIdentitySting#keys-1",
        
        // The DID Verification method
        "type": "Ed25519VerificationKey2018",
        
        // DID for person who controllers this DID Document
        "controller": "did:example:hemisDIDIdentitySting",
        
        // A public key encoded according to [MULTICODEC] and formatted according to [MULTIBASE].
        "publicKeyMultibase": "multibaseEncodeOfExampleHemisPublicKeyString"
      }],
      
      // Used for Authentication purposes such as logging into a website or engaging in any sort of challenge-response protocol.
      "authentication": [
          "did:example:hemisDIDIdentitySting#keys-1"
      ],
      
      // Used to express claims, such as issuing a Verifiable Credential
      "assertionMethod": [
          "did:example:hemisDIDIdentitySting#keys-1"
      ],
      
      // Used for establishing a secure communication channel with the recipient.
      "keyAgreement": [
          "did:example:hemisDIDIdentitySting#keys-1"
      ],
      
      // Used to delegate cryptographic authority such as authorization to access an API.
      "capabilityDelegation": [
          "did:example:hemisDIDIdentitySting#keys-1"
      ],
      
      // Used to invoke cryptographic capability such as authority to edit the DID Document
      "capabilityInvocation": [
          "did:example:hemisDIDIdentitySting#keys-1"
      ]
    }
```
Once the DID and DID document has been created with the private keys in control of the person on their device we can now use our wallet for authenication and for VC Processes 

### VC Processes
1. Issuance of one or more verifiable credentials.
2. Storage of verifiable credentials in a credential repository (such as a digital wallet).
3. Composition of verifiable credentials into a verifiable presentation for verifiers.
4. Verification of the verifiable presentation by the verifier.

To illustrate this lifecycle, we will use the example of verified registration for a subtribe. In the example below, Hemi receives a Iwi member verifiable credential from a Ngāti Iwi, and Hemi stores the verifiable credential in a digital wallet.

**TRIBAL MEMBER VC EXAMPLE**
```
{ 
    // set the context, which establishes the special terms we will be using 
    // such as 'issuer' and 'memberOf'. 
    "@context": [ 
        "https://www.w3.org/2018/credentials/v1", 
        https://www.w3.org/2018/credentials/examples/v1" 
    ], 
    
    // specify the identifier for the credential 
    "id": "http://credentials.maori.nz/member/v1", 
    
    // the credential types, which declare what data to expect in the credential 
    "type": ["VerifiableCredential", "TribalMember"], 
    
    // the entity that issued the credential 
    "issuer": "did:example:MoanaDIDIdentifierNumber", 
    
    // when the credential was issued 
    "issuanceDate": "2023-01-01T19:23:24Z", 
    
    // claims about the subjects of the credential 
    "credentialSubject": { 
        
        // identifier for the only subject of the credential 
        "id": "did:example:hemiDIDIdentitfierNumber", 
        
        // assertion about the only subject of the credential 
        "memberOf": { 
            "id": "did:example:iwiDIDIdentifierNumber", 
            "name": "Ngati Iwi",
        } 
    }, 
    // digital proof that makes the credential tamper-evident 
    "proof": { 
        // the cryptographic signature suite that was used to generate the signature 
        "type": "Ed25519Signature2020",
        
        // the date the signature was created 
        "created": "2023-02-25T14:58:42Z",
        
        
        // purpose of this proof 
        "proofPurpose": "assertionMethod", 
        
        // the identifier of the public key that can verify the signature 
        "verificationMethod": "did:example:iwiDIDIdentifierNumber#Verificationkey-1", 
        
        // A detached EdDSA produced according to [RFC8032], encoded according to [MULTIBASE].
        "proofValue": "exampleDetachedEdDSACreatedFromTheIwiPrivateKeyThatCanBeDrivedWithTheIwiPublicKeyString"
        
    } 
}
```
> Define what the detached EdDSA is?

Hemi then attempts to register with his Iwi. The Iwi registration form states that any member of "Ngati Hapu" does not need to provide whakapapa information. Hemi starts the process of registration and is asked if he would like to register with Wallet. The digital wallet asks Hemi if he would like to provide a previously issued verifiable credential. Hemi selects the Hapū verifiable credential, which is then composed into a verifiable presentation. The verifiable presentation is sent to the verifier and verified.

**TRIBAL MEMBER VC REPRESENTATION EXAMPLE**
```

{
    "@context": [
        "https://www.w3.org/2018/credentials/v1",
        "https://www.w3.org/2018/credentials/examples/v1"
    ],
    "type": "VerifiablePresentation",
    
    // the verifiable credential issued in the previous example
    "verifiableCredential": [{ .... insert TRIBAL MEMBER VC EXAMPLE..... }],

    // digital signature by Hemi on the presentation
    // protects against replay attacks
    "proof": { 
        "type": "Ed25519Signature2020",
        "created": "2023-03-25T14:58:42Z",
        "proofPurpose": "authenication", 
        // the identifier of the public key that can verify the signature 
        "verificationMethod": "did:example:hemiDIDIdentifierNumber#Verificationkey-1", 

        // the digital signature value 
        "proofValue": "exampleDetachedEdDSACreatedFromHemisPrivateKeyThatCanBeDrivedWithTheIwiPublicKeyString"

    } 
}
```
---
## Context
- [Detailed Ahau VC process: WIP](https://docs.google.com/document/d/1hEN9QmFdpOPPZDttJqbre5LUFbPY5rebTROt86wNqh0/edit#heading=h.5ky6t3sxdiro)
- [Ahau: TribalDID's Cardano Grant](https://cardano.ideascale.com/c/idea/422853) 
- [W3C Verifiable Credentials](https://www.w3.org/TR/vc-data-model/)
- [W3C Decentralised identifiers](https://www.w3.org/TR/did-core/)
- [W3C DID Implementation Guide](https://www.w3.org/TR/did-imp-guide/)
- [W3C DID Method Rubic](https://w3c.github.io/did-rubric/)
- [DID Archetecture](https://i.imgur.com/4wNSJAb.png)
    - Overview of DID architecture and the relationship of the basic components - https://www.w3.org/TR/did-core/#architecture-overview

---

## Notes

DID's are a fancy ID number
DID Documents define the 
- *Controller* of the DID (who can make changes to the DID)
- *Subject* of the DID (you or your child, your org, your pet, your book)
- *Cryptographic data* Different cryptographic keys used of 
    - authentication
    - assertions of credentials
    - key agreements for secure comms
    - authorisation for access to API
    - delegate access to other authorities

DID Documents can include other information 
DID documents can digitally exist and be stored in a DB somewhere or they can be generated on the fly when needed

DID's can be useful by themselves as unique ID numbers
- so you know which John Smith wrote the book
- which bottle of wine was brought
- which tree fell over 

DID's are often linked to VC's
- an unforgable credential proving university degree linked to John Smiths DID


Verfiable Data Registry: 
- Cardano chain?
